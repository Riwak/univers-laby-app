# README #

Jeu iOS et application MacOS/ Windows.
Mise en place d'un algorithme optimisé pour la création et génération de labyrinthes. Résolution automatique et Gameplay amélioré.

Dans ce lieu de perdition, on ne peut survivre longtemps. Dans un espace où les possibilités de mouvement sont rendues indéchiffrables, le corps et l'esprit s'épuisent. Si par ailleurs cet espace est peuplé de monstres et pavé de pièges mortels, l'épreuve devient infernale. Il est cependant possible d'imaginer et déployer des stratégies qui permettent de trouver son chemin et d'éviter les embûches. 

### Version ###

* V2.1
* Codé en C & C++ pour la version desktop
* PhoneGap et Web pour la version mobile

### About ###

EFREI : Projet Structure de Donnée SDD - L2 - 2013